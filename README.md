# Urban Slinger - DS game

This game is a final project for the EE-310 course at EPFL

## How to play

The game should start paused. to start it, press the START.

This game requires you to hold the Nintendo DS upside down,  
your goal is to survive as long as possible.

Tap the screen to swing to a wall, and use the A + Y keys to turn the camera.

The game ends if you fall too low or hit a wall.

## Controls
START - Pause/Resume
SELECT - Restart when paused.

A - steer camera left
Y - steer camera right

Touch Screen - Swing to the spot you clicked

X - Slow down the game for 2 seconds (cooldown: 5s)

